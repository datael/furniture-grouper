-------------------------------------------------------------------------------
-- Furniture Grouper Init
-- Datael (@Datael) - 2017
-------------------------------------------------------------------------------

FurnitureGrouper = {
	name = "FurnitureGrouper",
}

FurnitureGrouper_Mover = {}

FurnitureGrouperConstants = {
	GradualMoveDelay = 50,
	ResetDelayAfterForcedPlacement = 250,
	Dialogs = {
		ClearGroupConfirm = "FurnitureGrouper_Dialogs_ClearGroupConfirm",
		ResetPositionsConfirm = "FurnitureGrouper_Dialogs_ResetPositionsConfirm",
	},
}
