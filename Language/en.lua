﻿-------------------------------------------------------------------------------
-- Furniture Grouper EN
-- Datael (@Datael) - 2017
-------------------------------------------------------------------------------

-- Controls

ZO_CreateStringId("SI_BINDING_NAME_FURNITURE_GROUPER_ADD_TO_GROUP", "Add highlighted furniture to group")
ZO_CreateStringId("SI_BINDING_NAME_FURNITURE_GROUPER_REMOVE_FROM_GROUP", "Remove highlighted furniture from group")
ZO_CreateStringId("SI_BINDING_NAME_FURNITURE_GROUPER_CLEAR_GROUP", "Clear all items from group")
ZO_CreateStringId("SI_BINDING_NAME_FURNITURE_GROUPER_RESET_POSITIONS", "Reset position of all items in group")

-- Keybind Strip

ZO_CreateStringId("SI_FURNITURE_GROUPER_ADD_TO_GROUP_STRIP_LABEL", "Add to Group")
ZO_CreateStringId("SI_FURNITURE_GROUPER_REMOVE_FROM_GROUP_STRIP_LABEL", "Remove from Group")
ZO_CreateStringId("SI_FURNITURE_GROUPER_CLEAR_GROUP_STRIP_LABEL", "Clear Group")
ZO_CreateStringId("SI_FURNITURE_GROUPER_RESET_GROUP_STRIP_LABEL", "Reset Positions")

-- Dialogs

ZO_CreateStringId("SI_FURNITURE_GROUPER_CLEAR_GROUP_DIALOG_TITLE", "Clear Group?")
ZO_CreateStringId("SI_FURNITURE_GROUPER_CLEAR_GROUP_DIALOG_BODY", "Are you sure you want to remove all furniture from the group?")

ZO_CreateStringId("SI_FURNITURE_GROUPER_RESET_POSITIONS_DIALOG_TITLE", "Reset Positions?")
ZO_CreateStringId("SI_FURNITURE_GROUPER_RESET_POSITIONS_DIALOG_BODY", "Are you sure you want to return all furniture to their original positions?")
