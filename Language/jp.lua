﻿-------------------------------------------------------------------------------
-- Furniture Grouper JP
-- Datael (@Datael) - 2017
-------------------------------------------------------------------------------

-- Controls

ZO_CreateStringId("SI_BINDING_NAME_FURNITURE_GROUPER_ADD_TO_GROUP", "ハイライトされている家具をグループに追加")
ZO_CreateStringId("SI_BINDING_NAME_FURNITURE_GROUPER_REMOVE_FROM_GROUP", "ハイライトされている家具をグループから除去")
ZO_CreateStringId("SI_BINDING_NAME_FURNITURE_GROUPER_CLEAR_GROUP", "家具のグループを解除")
ZO_CreateStringId("SI_BINDING_NAME_FURNITURE_GROUPER_RESET_POSITIONS", "グループされている家具の位置を元に戻す")

-- Keybind Strip

ZO_CreateStringId("SI_FURNITURE_GROUPER_ADD_TO_GROUP_STRIP_LABEL", "グループに追加")
ZO_CreateStringId("SI_FURNITURE_GROUPER_REMOVE_FROM_GROUP_STRIP_LABEL", "グループから除去")
ZO_CreateStringId("SI_FURNITURE_GROUPER_CLEAR_GROUP_STRIP_LABEL", "グループ解除")
ZO_CreateStringId("SI_FURNITURE_GROUPER_RESET_GROUP_STRIP_LABEL", "グループを元に戻す")

-- Dialogs

ZO_CreateStringId("SI_FURNITURE_GROUPER_CLEAR_GROUP_DIALOG_TITLE", "グループ解除")
ZO_CreateStringId("SI_FURNITURE_GROUPER_CLEAR_GROUP_DIALOG_BODY", "グループを解除します。よろしいですか？")

ZO_CreateStringId("SI_FURNITURE_GROUPER_RESET_POSITIONS_DIALOG_TITLE", "元の位置に戻す")
ZO_CreateStringId("SI_FURNITURE_GROUPER_RESET_POSITIONS_DIALOG_BODY", "家具をすべて、グループに追加する前の位置に戻します。よろしいですか？")
